import React from 'react';
import ReactDOM from 'react-dom';
import App from './app';
import {BrowserRouter as Router} from 'react-router-dom';
import registerServiceWorker from './registerServiceWorker';
import 'bootstrap/dist/css/bootstrap.css';
import 'font-awesome/css/font-awesome.css';
import './custom.css';
ReactDOM.render(
        <Router>
            <App />
        </Router>, document.getElementById('root'));
registerServiceWorker();
