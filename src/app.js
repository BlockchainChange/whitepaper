import React, { Component } from 'react';
import { Switch, Route } from 'react-router';
import { withRouter, Link } from 'react-router-dom';
import Sidebar from './components/sidebar';
import MainMenu from './components/main-menu';
import BodyContent from './components/body-content';
import Footer from './components/footer';
import PrivacyPolicy from './components/privacy-policy';
import TermsOfService from './components/terms-of-service';
import Introduction from './components/introduction';
import Wallet from './components/wallet';
import Nodes from './components/nodes';
import Mining from './components/mining';
import OtherSoftware from './components/other-software';
import DeveloperAPI from './components/developer-api';

import { Container, Row, Col, Alert } from 'reactstrap';
class App extends Component {
  constructor(props) {
      super(props);
      document.title = 'Blockchain Change Whitepaper'
      this.state = {
        alert : {
          '0' : {
            isVisible : true
          }
        }
      }
  }

  componentWillMount(){
    const closeAlert = () => {
      this.setState({alert : { '0' : { isVisible : !this.state.alert[0].isVisible}}})
    };
    setTimeout(closeAlert, 5000);
  }

  setBreadcrumbPage(page = 'Welcome') {
    this.breadcrumbLocation = page;
  }

  getBreadcrumbLocation(){
    return this.breadcrumbLocation;//<Link to={this.props.location.pathname}>Other</Link>;
  }

  render() {
    this.setBreadcrumbPage(this.props.location.pathname.substr(1));
    return (
      <Container fluid className="no-gutters px-0 bg-light h-100">
        <Row className="h-100 no-gutters">
          <Col size={2} md={2} className="h-100 d-none d-md-block d-lg-block d-xl-block">
            <Sidebar />
          </Col>
          <Col size={10} md={10} className="py-1 bg-dark text-info justify-content-center">

            <Alert color="primary" className="text-center mt-3 mx-2" isOpen={this.state.alert[0].isVisible} toggle={()=>{ this.setState({ alert : { '0': { isVisible : !this.state.alert[0].isVisible } }})}}>
              <h3><strong>Welcome to Blockchain Change Initiative<br className="d-block d-sm-none"/> Whitepaper</strong></h3>
            </Alert>
            <span className="float-left pl-3"><small><Link to="/">Whitepaper</Link> > {this.props.location.pathname === "/" ? <Link to="/">Home</Link> : this.getBreadcrumbLocation()}</small></span>
            <br/>
            <MainMenu /> 
            <Switch>
              <Route path="/" component={BodyContent} exact />
              <Route path="/Introduction"  component={Introduction} exact />
              <Route path="/Wallet" component={Wallet} exact />
              <Route path="/Nodes" component={Nodes} exact />
              <Route path="/Mining" component={Mining} exact />
              <Route path="/Privacy-Policy" component={PrivacyPolicy} exact />
              <Route path="/Terms-Of-Service" component={TermsOfService} exact />
              <Route path="/Other-Software" component={OtherSoftware} exact />
              <Route path="/Developer-API" component={DeveloperAPI} exact />
            </Switch>
            <Footer />
          </Col>
        </Row>
      </Container>
    );
  }
}

export default withRouter(App);
