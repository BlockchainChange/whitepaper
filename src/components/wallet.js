import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { 
    Container, 
    Row,
    Col, 
    Card, 
    CardHeader, 
    CardTitle,  
    CardBody, 
    CardFooter
} from 'reactstrap';
import BlockExplorer28 from '../assets/Block Explorer -- Blockchain Change (27).svg';
import BlockExplorer29 from '../assets/Block Explorer -- Blockchain Change (28).svg';
import BlockExplorer30 from '../assets/Block Explorer -- Blockchain Change (29).svg';
import BlockExplorer31 from '../assets/Block Explorer -- Blockchain Change (30).svg';
import BlockExplorer32 from '../assets/Block Explorer -- Blockchain Change (31).svg';

class WalletInterface extends Component {
    render() {
        return (
            <Container fluid className="no-gutters py-3 bg-light">
                <Row className="mx-auto justify-content-center my-2">
                    <Col md={12} className="align-self-center text-center">
                        <h1>Blockchain Change Initiative</h1>
                        <h3>Wallet Interface</h3>
                    </Col>
                </Row>
                <Row className="mx-auto justify-content-center my-5 no-gutters">
                    <Col md={4} className="align-self-top text-center">
                        <h3 className="text-primary">Message's</h3>
                        <div className="h-100">
                            <p className="px-3 py-3 text-justify">
                                With our Wallet Interface we make sending messages on the blockchain just as easy as sending money.
                            </p>
                        </div>
                    </Col>
                    <Col md={4} className="align-self-top text-center">
                        <h3 className="text-primary">Secure</h3>
                        <div className="h-100">
                            <p className="px-3 py-3 text-justify">
                                Using trusted sources of security like RSA Signatures, and Bcrypt encryption we form our blockchain with security in mind.
                            </p>
                        </div>
                    </Col>
                    <Col md={4} className="align-self-top text-center">
                        <h3 className="text-primary">Open Source</h3>
                        <div className="h-100">
                            <p className="px-3 py-3 text-justify">
                                Our project is offered under the standard MIT License, making it available for everyone. Come join in and make BCCI great!
                            </p>
                        </div>
                    </Col>
                </Row>
                <Row className="justify-content-center bg-dark">
                    <Col md={12} className="align-self-center text-center text-primary py-5">
                        <h3>Create your Wallet</h3>
                    </Col>
                </Row>
                <Row className="my-5 justify-content-center no-gutters">
                    <Col sm={12} md={6} className="align-self-center">
                        <Row className="justify-content-center no-gutters p-3">
                            <Col md={10} className="border border-warning p-3 ">
                                <p className="text-justify">Start out by creating your wallet key. What is the wallet key? The key is just one level of security we use to secure our chain. We use an RSA Key pair, the private key is used to sign, while the public is used as the address. We use it to sign and verify digital transactions.</p>
                            </Col>
                        </Row>
                    </Col>
                    <Col sm={12} md={6} className="align-self-center">
                        <Col md={10} className="p-3">
                            <img className="img-fluid" src={BlockExplorer28} />
                        </Col>
                    </Col>
                </Row>  
                <Row className="justify-content-center no-gutters">
                    <Col md={4} className="align-self-top text-center">
                        <h3 className="text-primary">Customizable</h3>
                        <div className="h-100">
                            <p className="px-3 py-3 text-justify">
                                You can download the Wallet Interface and make it your own. It's merely an interface to interact with the Blockchain API.
                            </p>
                        </div>
                    </Col>
                    <Col md={4} className="align-self-top text-center">
                        <h3 className="text-primary">Simple Design</h3>
                        <div className="h-100">
                            <p className="px-3 py-3 text-justify">
                                Blockchain is not that simple to understand. The BCCI has tried to make it as simple as we can to interact with our blockchain.
                            </p>
                        </div>
                    </Col>
                    <Col md={4} className="align-self-top text-center">
                        <h3 className="text-primary">Open Source</h3>
                        <div className="h-100">
                            <p className="px-3 py-3 text-justify">
                                Our project is offered under the standard MIT License, and we encourage others to join the initiative.
                            </p>
                        </div>
                    </Col>
                </Row>

                <Row className="justify-content-center bg-dark">
                    <Col md={12} className="align-self-center text-center text-primary my-5">
                        <h2>Wallet Address</h2>
                        <h3><sub>Public Key</sub></h3>
                    </Col>
                </Row>
                
                <Row className="my-5 justify-content-center no-gutters">
                    <Col sm={12} md={6} className="align-self-center">
                        <Col md={10} className="p-3">
                            <img className="img-fluid" src={BlockExplorer29} />
                        </Col>
                    </Col>
                    <Col sm={12} md={6} className="align-self-center">
                        <Row className="justify-content-center no-gutters p-3">
                            <Col md={10} className="border border-warning p-3 ">
                                <p className="text-justify">Your public key is the actual address used on the blockchain. No need to store it, as it's part of your private key. You will have the chance to download the qr-code later, it stores the wallet address for easy reference.</p>
                            </Col>
                        </Row>
                    </Col>
                </Row>  

                
                <Row className="justify-content-center bg-dark">
                    <Col md={12} className="align-self-center text-center text-primary my-5">
                        <h2>Private Key</h2>
                        <h3><small><sub className="text-danger">Keep this safe!</sub></small></h3>
                    </Col>
                </Row>

                <Row className="my-5 justify-content-center no-gutters">
                    <Col sm={12} md={6} className="align-self-center">
                        <Row className="justify-content-center no-gutters p-3">
                            <Col md={10} className="border border-warning p-3 ">
                                <p className="text-justify">Your private key is used to sign transactions on the block. It's how we authorize and validate interactions with the wallet. <span className="text-warning">KEEP IT SAFE!</span> If someone gain's access to your private key, they can sign transactions, just as if they were you.</p>
                            </Col>
                        </Row>
                    </Col>
                    <Col sm={12} md={6} className="align-self-center">
                        <Col md={10} className="p-3">
                            <img className="img-fluid" src={BlockExplorer30} />
                        </Col>
                    </Col>
                </Row>  
                
                <Row className="justify-content-center bg-dark">
                    <Col md={12} className="align-self-center text-center text-primary my-5 py-5">
                        <h3>Sign In</h3>
                    </Col>
                </Row>
                
                <Row className="my-5 justify-content-center no-gutters">
                    <Col sm={12} md={6} className="align-self-center">
                        <Col md={10} className="p-3">
                            <img className="img-fluid" src={BlockExplorer31} />
                        </Col>
                    </Col>
                    <Col sm={12} md={6} className="align-self-center">
                        <Row className="justify-content-center no-gutters p-3">
                            <Col md={10} className="border border-warning p-3 ">
                                <p className="text-justify">Once you have saved your key some where safe, maybe make a backup copy just in case! You will need your key to send transactions on the blockchain. If you loose it, no one can recover it for you. It will be lost forever.</p>
                            </Col>
                        </Row>
                    </Col>
                </Row>  

                   
                <Row className="justify-content-center bg-dark">
                    <Col md={12} className="align-self-center text-center text-primary my-5 py-5">
                        <h3>Wallet Interface</h3>
                    </Col>
                </Row>         

                <Row className="my-5 justify-content-center no-gutters">
                    <Col sm={12} md={6} className="align-self-center">
                        <Row className="justify-content-center no-gutters p-3">
                            <Col md={10} className="border border-warning p-3">
                                <p className="text-justify">The wallet explorer have several features, including listing your sent and received transactions, as well as mined blocks and your current balance. With a few clicks you can also download your qr-code as well as send transactions.</p>
                            </Col>
                        </Row>
                    </Col>
                    <Col sm={12} md={6} className="align-self-center">
                        <Col md={10} className="p-3">
                            <img className="img-fluid" src={BlockExplorer32} />
                        </Col>
                    </Col>
                </Row>  
            </Container>
        );
    }
}

export default withRouter(WalletInterface);