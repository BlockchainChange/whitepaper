import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';

import { 
    Container, 
    Row, 
    Col, 
    Card,
    CardHeader, 
    UncontrolledCarousel, 
    CardTitle, 
    CardSubtitle, 
    CardText, 
    CardFooter, 
    Jumbotron
} from 'reactstrap';

class BodyContent extends Component {
    render() {
        return (
            [
                <UncontrolledCarousel key={`main_carousel`} items={[{
                    altText: 'Blockchain Chage Initiative, bringing change to blockchain.',
                    caption: 'Bringing change to the blockchain.',
                    header: 'Blockchain Change Initiative', 
                    src:'//placehold.it/800x300?text=Creating Change'},
                    {
                        altText: 'Customize your experience by making the wallet interface you own.',
                        caption: 'Make it your own.',
                        header: 'Customizable Wallet Interface', 
                        src:'//placehold.it/800x300?text=Your Style'},
                    {
                    altText: 'JavaScript Built and Driven Blockchain',
                    caption: 'JavaScript Built and Driven Blockchain',
                    header: 'Not A Clone', 
                    src:'//placehold.it/800x300?text=Open Source License'}]} />,    
            <Container fluid className="text-center bg-secondary" key={`top_section`}>
                <section>
                    <Row className="text-white">
                        <Col>
                            <Jumbotron className="mx-2 my-4 text-info">
                                <h3>Blockchain Change Initiative</h3>
                                <p>Bringing Change to the Blockchain</p>
                            </Jumbotron>
                        </Col>
                    </Row>
                </section>
            </Container>,
            <Container fluid className="mx-auto py-2 text-center bg-info" key={`thumbnail_type_a`}>
                <section>
                    <Row className="justify-content-center text-white">
                        <Col>
                            <h3>Using blockchain to build the future.</h3>
                            <h4>Blockchain driven development.</h4>
                            <Row className="my-4 text-info">
                                <Col size={4} sm={12} md={4}>
                                    <Card>
                                        <CardHeader>
                                            <CardTitle>Decentralized Data</CardTitle>
                                            <img alt="" className="img-thumbnail img-responsive" src="//placehold.it/175x175?text=card1"/>
                                            <CardText className="text-justify">
                                                By decentralizing data of all type's we allow everyone the chance to participate in the information age. 
                                                We all own the knowledge we should give everyone access to it. 
                                            </CardText>
                                        </CardHeader>
                                        <CardFooter>
                                            <CardSubtitle>Blockchain driven decentralization.</CardSubtitle>
                                        </CardFooter>
                                    </Card>
                                </Col>
                                <Col size={4} sm={12} md={4}>
                                    <Card>
                                        <CardHeader>
                                            <CardTitle>Smart Contracts</CardTitle>
                                            <img alt="" className="img-thumbnail img-responsive" src="//placehold.it/175x175?text=card2"/>
                                            <CardText className="text-justify">
                                                From a simple contract to a fully customizable smart contract, you can build the future of your application stored on a network of decentralized servers.
                                            </CardText>
                                        </CardHeader>
                                        <CardFooter>
                                            <CardSubtitle>JavaScript Driven Development</CardSubtitle>
                                        </CardFooter>
                                    </Card>
                                </Col>
                                <Col size={4} sm={12} md={4}>
                                    <Card>
                                        <CardHeader>
                                            <CardTitle>Open Source License</CardTitle>
                                            <img alt="" className="img-thumbnail img-responsive" src="//placehold.it/175x175?text=card3"/>
                                            <CardText className="text-justify">
                                                We should all have the right to learn, and to have access to information. Decentralization of data will lead to everyone having access to books and other information, leading to the choice of education.
                                            </CardText>
                                        </CardHeader>
                                        <CardFooter>
                                            <CardSubtitle>Give and you will get in return!</CardSubtitle>
                                        </CardFooter>
                                    </Card>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </section>
            </Container>,
            <Container fluid className="mx-auto py-2 text-center bg-primary" key={`banner_type_a`}>
                <section>
                    <Row>
                        <Col>
                            <h3 className="my-3 py-2 text-white">Blockchain integrated development is on the rise.</h3>
                            <Jumbotron className="mx-2 my-4 text-primary">
                                <Row>
                                    <Col>
                                            Reward Systems
                                    </Col>
                                    <Col>
                                            * 
                                    </Col>
                                    <Col>
                                            Shopping Carts
                                    </Col>
                                    <Col>
                                            *
                                    </Col>
                                    <Col>
                                            Message Apps
                                    </Col>
                                </Row>
                            </Jumbotron>
                            <h4 className="my-3 py-1 text-white">Technology moving into the future.</h4>
                        </Col>
                    </Row>
                </section>
            </Container>,
            <Container fluid className="mx-auto py-2 text-center bg-info" key={`thumbnail_type_b`}>
                <section>
                    <Row className="justify-content-center text-white">
                        <Col>
                            <h3>Explore how blockchain can change the way you handle data.</h3>
                            <h4>Decentralized data storage across your network.</h4>
                            <Row className="my-4 text-info">
                                <Col size={4} sm={12} md={4}>
                                    <Card>
                                        <CardHeader>
                                            <CardTitle>Simple</CardTitle>
                                            <img alt="" className="img-thumbnail img-responsive rounded-circle" src="//placehold.it/175x175?text=card1"/>
                                            <CardText>
                                                We strive to make it simple, and easy to use for everyone.
                                            </CardText>
                                        </CardHeader>
                                        <CardFooter>
                                            <CardSubtitle>Join the discussion to learn more.</CardSubtitle>
                                        </CardFooter>
                                    </Card>
                                </Col>
                                <Col size={4} sm={12} md={4}>
                                    <Card>
                                        <CardHeader>
                                            <CardTitle>Secure</CardTitle>
                                            <img alt="" className="img-thumbnail img-responsive rounded-circle" src="//placehold.it/175x175?text=card2"/>
                                            <CardText>
                                                Building secure with RSA Signatures and progressive hashing.
                                            </CardText>
                                        </CardHeader>
                                        <CardFooter>
                                            <CardSubtitle>Bcrypt / RSA / Crypto Built In</CardSubtitle>
                                        </CardFooter>
                                    </Card>
                                </Col>
                                <Col size={4} sm={12} md={4}>
                                    <Card>
                                        <CardHeader>
                                            <CardTitle>Unique</CardTitle>
                                            <img alt="" className="img-thumbnail img-responsive rounded-circle" src="//placehold.it/175x175?text=card3"/>
                                            <CardText>
                                                We are not merely a coin, but a way to decentralize data.
                                            </CardText>
                                        </CardHeader>
                                        <CardFooter>
                                            <CardSubtitle>Send coin, messages and more.</CardSubtitle>
                                        </CardFooter>
                                    </Card>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </section>
            </Container>,
            <Container fluid className="text-center bg-secondary" key={`banner_section`}>
                <section>
                    <Row className="text-white">
                        <Col>
                            <Jumbotron className="mx-2 my-4 text-info">
                                <h3>Blockchain, a natural evolution to your data management needs.</h3>
                            </Jumbotron>
                        </Col>
                    </Row>
                </section>
            </Container>
        ]
        );
    }
}
  
export default withRouter(BodyContent);
