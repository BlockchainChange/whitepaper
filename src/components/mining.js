import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { 
    Container, 
    Row,
    Col, 
    Card, 
    CardHeader, 
    CardTitle,  
    CardBody, 
    CardFooter
} from 'reactstrap';

class Mining extends Component {
    render() {
        return (
            <Container fluid className="no-gutters px-0 py-3 bg-light">
                <Container>
                    <Row className="align-content-center justify-content-center my-2">
                        <Col md={10} className="align-self-center">
                            <Card>
                                <CardHeader>
                                    <CardTitle>Mining Blockchain Change</CardTitle>
                                </CardHeader>
                                <CardBody>
                                </CardBody>
                                <CardFooter>
                                    Mining the blockchain starts by working with a node. As its the miners who gather information off the node, and then package that information in to blocks.

                                    How does it work, well the mining applications will contact the node api, grab up to 99 transactions that have been sent to be recorded. They go through each one verifying the signatures and hashes. Once it has verified its time to package it up.
                                    The miner takes the transaction and creates a block, adding them to the transactions section. Then it takes all that information creates another hash and signs the block. Once this is complete it sends it to the node who send it off for confirmation.
                                    Once confirmation is complete the node then adds the new block to the chain.

                                </CardFooter>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </Container>
        );
    }
}

export default withRouter(Mining);