import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { 
    Container, 
    Row,
    Col,
    CardDeck,
    Card
} from 'reactstrap';
import BlockExplorer from '../assets/Block Explorer -- Blockchain Change.svg';
import BlockExplorer1 from '../assets/Block Explorer -- Blockchain Change (1).svg';


class Introduction extends Component {
    render() {
        return (
            <Container fluid className="no-gutters px-0 py-3 bg-light pb-1">
                <Row className="mx-auto justify-content-center my-2">
                    <Col md={12} className="align-self-center text-center">
                        <h1><strong>Introducing</strong><br/> The Blockchain Change Initiative</h1>
                        <h3>AKA The B.C.C.I.</h3>
                    </Col>
                </Row>
                <Row className="mx-auto justify-content-center my-5 px-3">
                    <Col md={4} className="align-self-top text-center">
                        <h3 className="text-primary">Simple</h3>
                        <div className="h-100">
                            <p className="px-3 py-3 text-justify">
                                With our Wallet Interface we make sending transactions simple. If you dont like ours, your welcome to copy it and make it your own!
                            </p>
                        </div>
                    </Col>
                    <Col md={4} className="align-self-top text-center">
                        <h3 className="text-primary">Secure</h3>
                        <div className="h-100">
                            <p className="px-3 py-3 text-justify">
                                Using trusted sources of security like RSA Signatures, and Bcrypt encryption we form our blockchain with security in mind.
                            </p>
                        </div>
                    </Col>
                    <Col md={4} className="align-self-top text-center">
                        <h3 className="text-primary">Open Source</h3>
                        <div className="h-100">
                            <p className="px-3 py-3 text-justify">
                                Our project is offered under the standard MIT License, making it available for everyone. Come join in and make BCCI great!
                            </p>
                        </div>
                    </Col>
                </Row>
                <Row className="mx-auto justify-content-center my-3 bg-dark">
                    <Col md={12} className="align-self-center text-center text-primary py-5">
                        <h1>Join the conversation.</h1>
                        <h3>Discord | Twitter | LinkedIn</h3>
                    </Col>
                </Row>
                <Row className="my-5 justify-content-center no-gutters">
                    <Col sm={12} md={6} className="align-self-center">
                        <Row className="justify-content-center no-gutters p-3">
                            <Col md={10} className="border border-warning p-3 ">
                                <p className="text-justify">Blockchain Change Initiative is a blockchain community working to bring change to the block. We offer a unique, custom built blockchain. It was built totally using JavaScript and JavaScript frameworks and tools like ReactJS, NodeJS, and MongoDB.</p>
                            </Col>
                        </Row>
                    </Col>
                    <Col sm={12} md={6} className="align-self-center">
                        <Col md={10} className="p-3">
                            <img className="img-fluid" src={BlockExplorer} />
                        </Col>
                    </Col>
                </Row>  
                <Row className="mx-auto justify-content-center my-3 bg-dark">
                    <Col md={12} className="align-self-center text-center text-primary py-5">
                        <h3>Explore the chain with the Block Explorer, built right into the Wallet Interface.</h3>
                        <h3><sub>See messages and transaction's as they reach the blockchain.</sub></h3>
                    </Col>
                </Row>
                <Row className="my-5 justify-content-center no-gutters">
                    <Col sm={12} md={6} className="align-self-center">
                        <Row className="justify-content-center no-gutters p-3">
                            <Col md={10} className="border border-warning p-3 ">
                                <p className="text-justify">Simple navigation, with community links, node identification, and quick links to sign-in help you traverse the blockchain, and find information on where the conversation is taking place..</p>
                            </Col>
                        </Row>
                    </Col>
                    <Col sm={12} md={6} className="align-self-center">
                        <Col md={10} className="p-3">
                            <img className="img-fluid" src={BlockExplorer1} />
                        </Col>
                    </Col>
                </Row> 
            </Container>
        );
    }
}

export default withRouter(Introduction);