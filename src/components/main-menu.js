import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';
import { 
  Nav, 
  Navbar, 
  NavbarBrand,
  NavLink, 
  NavItem, 
  Collapse, 
  NavbarToggler 
} from 'reactstrap';
class MainMenu extends Component {  constructor(props){
  super(props);
    this.state = {
      collapsed:true
    }
  }

  toggleNavbar(){
    this.setState({
      collapsed:!this.state.collapsed
    })
  }
  
  render() {
    return (
      <Navbar color="secondary" expand="sm" className="navbar-dark mt-2">
          <NavbarBrand className="text-info  float-left" tag={Link} to="/">Whitepaper</NavbarBrand>
          <NavbarToggler onClick={this.toggleNavbar.bind(this)} className="mr-auto bg-info text-white float-right" />
          <Collapse isOpen={!this.state.collapsed} navbar>      
            <Nav navbar>
              <NavItem>
                <NavLink tag={Link} to="/Introduction">Introduction</NavLink>
              </NavItem>
              <NavItem>
                <NavLink tag={Link} to="/Nodes">Nodes</NavLink>
              </NavItem>
              <NavItem>
                <NavLink tag={Link} to="/Wallet">Wallet Interface</NavLink>
              </NavItem>
              <NavItem>
                <NavLink tag={Link} to="/Mining">Mining</NavLink>
              </NavItem>
              <NavItem>
                <NavLink tag={Link} to="/Other-Software">Other Software</NavLink>
              </NavItem>
              <NavItem>
                <NavLink tag={Link} to="/Developer-API">Developer API</NavLink>
              </NavItem>
            </Nav>
          </Collapse>
        </Navbar>
    );
  }
}

export default withRouter(MainMenu);
