import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { 
    Container, 
    Row,
    Col, 
    Card, 
    CardHeader, 
    CardTitle,  
    CardBody, 
    CardFooter
} from 'reactstrap';

class Nodes extends Component {
    render() {
        return (
            <Container fluid className="no-gutters px-0 py-3 bg-light pb-1">
                <Row className="mx-auto justify-content-center my-2">
                    <Col md={12} className="align-self-center text-center">
                        <h1><strong>Nodes</strong><br/> Support the chain!</h1>
                        <h3>Each node adds to the decentralization of data!</h3>
                    </Col>
                </Row>
                <Row className="my-5 mx-auto">
                    <Col>
                        <Col md={10}>
                        Video Node's running...
                        </Col>
                    </Col>
                    <Col>
                        <Col md={10}>
                        <p className="text-justify">The node is the blockchain API. Each node stores a copy of the chain.
                         Using the blockchain API endpoints, interacting with the chain is simple as posting data to the node. From adding your Node, to creating a transactions, and blocks the API is made so you can build your own tools to interact with it.</p>
                        </Col>
                    </Col>
                </Row>
                <Row className="mx-auto justify-content-center my-2">
                    <Col md={12} className="align-self-center text-center text-primary">
                        <h1><strong>Nodes</strong><br/> Hold the data!</h1>
                        <h3>Spread the word, across the nodes!</h3>
                    </Col>
                </Row>
                <Row className="my-5 mx-auto">
                    <Col>
                        <Col md={10}>
                            <p className="text-justify">Each Node can have as many workers(miners) working to forge blocks on the chain. Since anyone can be a node, anyone can be setup to mine on the chain. Mining is how new coin are produced. You dont need to have a node to be a miner, and you dont need miners to be a node. Mine a friends node, or run your own.</p>
                        </Col>
                    </Col>
                    <Col>
                        <Col md={10}>
                        Video Node's Receiving Transactions.
                        </Col>
                    </Col>
                </Row>              
                <Row className="mx-auto justify-content-center my-2">
                    <Col md={12} className="align-self-center text-center">
                        <h1><strong>Nodes</strong><br/> On the Blockchain!</h1>
                        <h3>It cant be decentralized with just one, it takes many to become decentralized.</h3>
                    </Col>
                </Row>
            </Container>
        );
    }
}

export default withRouter(Nodes);