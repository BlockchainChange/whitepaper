import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { 
    Container, 
    Row,
    Col, 
    Card, 
    CardHeader, 
    CardTitle,  
    CardBody, 
    CardFooter
} from 'reactstrap';

class DeveloperAPI extends Component {
    render() {
        return (
            <Container fluid className="no-gutters px-0 py-3 bg-light">
                <Container>
                    <Row className="align-content-center justify-content-center my-2">
                        <Col md={10} className="align-self-center">
                            <Card>
                                <CardHeader>
                                    <CardTitle>Developer API Reference</CardTitle>
                                </CardHeader>
                                <CardBody>

                                Blockchainchange API Endpoint Reference    

                                <h3>Nodes</h3>
                                [get] \Nodes

                                    List currently available nodes.

                                [get] \Nodes\Status\:node

                                    Get the status of a known node.

                                [post] \Nodes
                                
                                    <code>{`{
                                        "address": String,
                                        "port": Number
                                    }`}</code>

                                    Add a node to the node list.
                                
                                <h3>Blocks</h3>
                                
                                [get] /Block
                                    Show the last 15 Blocks
                                
                                [get] /Block/Last-Block

                                    Show the last Block
                                
                                [get] /Block/Circulation-Totals

                                    Show the current Circulation Totals
                                
                                [get] /Block/{`{blockId}`}
                                
                                    Where Block Id is the block id in which you want to view.

                                [post] /Block Add a new block to the chain.
                                
                                    <code>{`{
                                        blockId: { type: Number, required:true, unique:true },
                                        forgedBy: { type: String, required:true },
                                        awardWallet: { type: String, required:true },
                                        awardTotal: { type: Number, required:true },
                                        difficulty: { type:Number, default: 2},
                                        timeComplexity: { type:Number, default: 2},
                                        previousHash: { type: String, required:true },
                                        hash: { type: String, required:true},
                                        transactions:[SEE Transactions ],
                                        proof: { type:Number, required:true},
                                        timestamp: { type: Number, required:true},
                                        signature: { type: { type: String }, data:[ Number ]}
                                    }`}</code>

                                <h3>Transactions</h3>
                                
                                [get] Transaction/To-Process
                                    
                                    Show a list of 100 transactions to be processes;

                                [get] Transaction/Last-Transaction
                                
                                    Show the last recorded transaction

                                [get] Transaction/LastFifteenTransactions
                                
                                    Show the last 15 Transactions

                                [get] Transaction/Index/:transactionIndex 
                                
                                    Show a transaction by index number.

                                Wallet

                                [get] Wallet/Balance

                                Requires Authorization: String

                                    String is a pkcs8 public pem key / wallet address
                                
                                    Error returns

                                    <code>{`{
                                    "status": "failed",
                                    "message": "A Public Key must be supplied"
                                    }`}</code>

                                [get] Wallet/Transaction

                                [post] Wallet/Transaction

                                    <code>{`{
                                        sender : 'rsa-pkcs8-public-key', 
                                        amount : Number,
                                        receiver : 'rsa-pkcs8-public-key',
                                        data : [DataSegment],
                                        proof : Number,
                                        timestamp: Date.now(),
                                        signature : {type:"Buffer", data:Array}
                                    }`}</code>

                                </CardBody>
                                <CardFooter>
                                    Have fun, and remember we are in it together!                                    
                                </CardFooter>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </Container>
        );
    }
}

export default withRouter(DeveloperAPI);