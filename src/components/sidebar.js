import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';
import { 
  Nav, 
  Navbar, 
  NavbarBrand, 
  NavLink, 
  NavItem,
  Button
} from 'reactstrap';
class Sidebar extends Component {
  render() {
    return (
      <Navbar fixed="bottom" color="light" className="col-md-2 justify-content-left">
          <NavbarBrand tag={Link} to="/">Whitepaper</NavbarBrand>
          <span className="pull-right">
          <Button color="info"><i className="fa fa-cog"></i> <span className="sr-only">Menu Toggle</span> </Button>
          </span>
          <Nav navbar className="col-12">
            <NavItem>
              <NavLink tag={Link} to="/Introduction" ><i className="img-thumbnail fa fa-comments text-primary"></i> Introduction<div className="align-self-center"></div></NavLink>
            </NavItem>
            <NavItem>
              <NavLink tag={Link} to="/Nodes" ><i className="img-thumbnail fa fa-ticket text-primary"></i> Nodes<div className="align-self-center"></div></NavLink>
            </NavItem>
            <NavItem>
              <NavLink tag={Link} to="/Block" ><i className="img-thumbnail fa fa-ticket text-primary"></i> Block Explorer<div className="align-self-center"></div></NavLink>
            </NavItem>
            <NavItem >
              <NavLink tag={Link} to="/Wallet"><i className="img-thumbnail fa fa-book text-primary"></i> Wallet Interface<div className="align-self-center"></div></NavLink>
            </NavItem>
            <NavItem>
              <NavLink tag={Link} to="/Mining" ><i className="img-thumbnail fa fa-comments text-primary"></i> Mining<div className="align-self-center"></div></NavLink>
            </NavItem>
            <NavItem>
              <NavLink tag={Link} to="/Other-Software" ><i className="img-thumbnail fa fa-comments text-primary"></i> Other Software<div className="align-self-center"></div></NavLink>
            </NavItem>
            <NavItem>
              <NavLink tag={Link} to="/Developer-API" ><i className="img-thumbnail fa fa-comments text-primary"></i> Developer API<div className="align-self-center"></div></NavLink>
            </NavItem>
          </Nav>
        </Navbar>
    );
  }
}

export default withRouter(Sidebar);
